Yii 2 Basic Project Template
============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-basic/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

СОДЕРЖАНИЕ
----------
1. DIRECTORY STRUCTURE
2. REQUIREMENTS
3. ОПИСАНИЕ
4. ОСОБЕННОСТИ РЕАЛИЗАЦИИ


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


ОПИСАНИЕ
--------

Реализация тестового задания https://docs.google.com/document/d/1BdYLJhPlhP9SA0V3um8pBth9D6hxlbArjKQAjaUfm0I/

Задача:

Сделать упрощенный сервис бронирования туров. Каждый тур состоит из названия. У каждого тура есть обязательный набор стандартных полей которые требуются при заполнении букинга. Эти поля одинаковы для всех туров в системе.
Это такие три поля как количество взрослых, детей, младенцев.

А также набор своих уникальных кастомных полей. Которые можно создать для каждого тура свои.

уточнение: При бронировании каждого тура нужно вводить требуемые данные, поля для этих данных бывают двух типов, стандартные которые одинаковы по названию для всех туров, и кастомные которые для каждого тура персонально. Но каждое поле в форме бронирования должно сортироваться для каждого тура именно так как было задано для этого тура.

Эти два типа полей должны иметь возможность сортировки как единого списка для определенного тура, и тогда при букинге тура эти поля показываются в соответствии с сортировкой. Букингов у тура может быть множество. У букинга есть дата на которую делается букинг.

По возможности использовать crud для скорости разработки. Использовать для создания/редактирования/удаления обьетов.

Писать на yii2, шаблон basic. База должна устанавливаться только их миграций, вендоры должны устанавливаться только через компосер с минимальной стабильностью stable. Код должен быть оформлен в соответствии со стилем кодирования и структурой каталогов в yii2.

Код выложите на какой нибудь онлайн репозиторий git.

Приоритет в разработке между скоростью и производительностью должен быть в пользу скорости разработки. Помните что неподдерживаемый и непонятный код для других разработчиков снижает скорость разработки. Нужно использовать готовые решения и возможности фреймворка.


ОСОБЕННОСТИ РЕАЛИЗАЦИИ
----------------------

В решении используется СУБД MySQL. Заданное в настройках название базы данных - booking.

Таблица стандартных полей является перечнем шаблонов полей при добавлении нового тура и в дальнейшей работе не используется.

Удалять стандартное поле из перечня полей тура запрещено. Для стандартного поля доступно только изменение порядка.

При редактировании заказа изменять тур запрещено.

Разграничение доступа по пользователям к функционалу не выполнялось.

Контроллеры, модели, представления стандартного шаблона не удалялись.

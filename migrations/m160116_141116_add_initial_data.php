<?php

use yii\db\Schema;
use yii\db\Migration;

class m160116_141116_add_initial_data extends Migration
{
    /*public function up()
    {

    }

    public function down()
    {
        echo "m160116_141116_add_initial_data cannot be reverted.\n";

        return false;
    }

    /**/
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
		$this->batchInsert('tours',
			['id', 'title'], [
			['1', 'Тур 1'],
			['2', 'Тур 2'],
		]);
		
		$this->batchInsert('standart_fields',
			['id', 'title', 'required', 'sort'], [
			['1', 'Количество взрослых', '1', '10'],
			['2', 'Количество детей', '1', '20'],
			['3', 'Количество младенцев', '1', '30'],
		]);
		
		$this->batchInsert('tour_fields',
			['id', 'tour_id', 'title', 'required', 'sort', 'standart'], [
			['1', '1', 'Количество взрослых', '1', '10', '1'],
			['2', '1', 'Количество детей', '1', '20', '1'],
			['3', '1', 'Количество младенцев', '1', '30', '1'],
			['4', '2', 'Количество взрослых', '1', '10', '1'],
			['5', '2', 'Количество детей', '1', '20', '1'],
			['6', '2', 'Количество младенцев', '1', '30', '1'],
			['7', '1', 'Доп. параметр 1', '0', '15', '0'],
			['8', '1', 'Доп. параметр 2', '1', '25', '0'],
			['9', '2', 'Доп. параметр 3', '0', '40', '0'],
		]);
		
		$this->batchInsert('bookings',
			['id', 'tour_id', 'book_date'], [
			['1', '1', '2016.01.16'],
			['2', '2', '2016.01.17'],
			['3', '2', '2016.01.16'],
		]);
		
		$this->batchInsert('book_params',
			['id', 'book_id', 'tour_field_id', 'value'], [
			['1', '1', '1', '1'],
			['2', '1', '2', '2'],
			['3', '1', '3', '0'],
			['4', '1', '7', 'Значение доп. параметра 1'],
			['5', '1', '8', 'Значение доп. параметра 2'],
			['6', '2', '4', '1'],
			['7', '2', '5', '2'],
			['8', '2', '6', '0'],
			['9', '2', '9', 'Значение доп. параметра 3'],
			['10', '3', '4', '2'],
			['11', '3', '5', '1'],
			['12', '3', '6', '1'],
			['13', '3', '9', 'Еще значение доп. параметра 3'],
		]);
    }

    public function safeDown()
    {
		
		echo "m160116_141116_add_initial_data data in tables must be cleared manualy.\n";
		
    }
    /**/
}

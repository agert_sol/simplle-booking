<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_182540_create_structure extends Migration
{
    /*public function up()
    {

    }

    public function down()
    {

	}

    /**/
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
		$this->createTable('tours', [
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING . ' NOT NULL',
		]);
		
		$this->createTable('standart_fields', [
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING . ' NOT NULL',
			'required' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
			'sort' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 10',
		]);
		
		$this->createTable('tour_fields', [
			'id' => Schema::TYPE_PK,
			'tour_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'title' => Schema::TYPE_STRING . ' NOT NULL',
			'required' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
			'sort' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 10',
			'standart' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
		]);
		
		$this->addForeignKey('tour_fields_tour_id_fk', 'tour_fields', 'tour_id', 'tours', 'id', 'CASCADE', 'CASCADE');
		
		$this->createTable('bookings', [
			'id' => Schema::TYPE_PK,
			'tour_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'book_date' => Schema::TYPE_DATE . ' NOT NULL',
		]);
		
		$this->addForeignKey('bookings_tour_id_fk', 'bookings', 'tour_id', 'tours', 'id', 'CASCADE', 'CASCADE');
		
		$this->createTable('book_params', [
			'id' => Schema::TYPE_PK,
			'book_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'tour_field_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'value' => Schema::TYPE_STRING . ' NOT NULL',
		]);
		
		$this->addForeignKey('book_params_book_id_fk', 'book_params', 'book_id', 'bookings', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('book_params_tour_field_id_fk', 'book_params', 'tour_field_id', 'tour_fields', 'id', 'CASCADE', 'CASCADE');
		
    }

    public function safeDown()
    {
		/*echo "m160115_182540_create_tables cannot be reverted.\n";
		return false;*/
		
		$this->dropTable('book_params');
		$this->dropTable('bookings');
		$this->dropTable('tour_fields');
		$this->dropTable('standart_fields');
		$this->dropTable('tours');
    }
    /**/
}

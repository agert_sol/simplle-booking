<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book_params".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $tour_field_id
 * @property string $value
 *
 * @property TourFields $tourField
 * @property Bookings $book
 */
class BookParams extends \yii\db\ActiveRecord
{
	
	public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['empty_value'] = $scenarios['default'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'tour_field_id', 'value'], 'required', 'on' => 'default'],
            [['book_id', 'tour_field_id'], 'required', 'on' => 'empty_value'],
            [['book_id', 'tour_field_id'], 'integer'],
            [['value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Код заказа',
            'tour_field_id' => 'Код поля тура',
            'value' => 'Значение поля',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourField()
    {
        return $this->hasOne(TourFields::className(), ['id' => 'tour_field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Bookings::className(), ['id' => 'book_id']);
    }
}

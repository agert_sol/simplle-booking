<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "standart_fields".
 *
 * @property integer $id
 * @property string $title
 * @property integer $required
 * @property integer $sort
 */
class StandartFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'standart_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['required', 'sort'], 'integer'],
            [['title'], 'string', 'max' => 255],
            ['required', 'default', 'value' => 1],
            ['sort', 'default', 'value' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название поля',
            'required' => 'Обязательное',
            'sort' => 'Сортировка',
        ];
    }
}

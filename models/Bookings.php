<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bookings".
 *
 * @property integer $id
 * @property integer $tour_id
 * @property string $book_date
 *
 * @property BookParams[] $bookParams
 * @property Tours $tour
 */
class Bookings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour_id', 'book_date'], 'required'],
            [['tour_id'], 'integer'],
            [['book_date'], 'safe'],
            ['book_date', 'date', 'format' => 'yyyy-M-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код заказа',
            'tour_id' => 'Код тура',
            'book_date' => 'Дата заказа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookParams()
    {
        return $this->hasMany(BookParams::className(), ['book_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tours::className(), ['id' => 'tour_id']);
    }
}

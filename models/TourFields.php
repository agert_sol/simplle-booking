<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tour_fields".
 *
 * @property integer $id
 * @property integer $tour_id
 * @property string $title
 * @property integer $required
 * @property integer $sort
 * @property integer $standart
 *
 * @property BookParams[] $bookParams
 * @property Tours $tour
 */
class TourFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tour_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour_id', 'title'], 'required'],
            [['tour_id', 'required', 'sort', 'standart'], 'integer'],
            [['title'], 'string', 'max' => 255],
            ['required', 'default', 'value' => 0],
            ['sort', 'default', 'value' => 10],
            ['standart', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'ID тура',
            'title' => 'Название',
            'required' => 'Обязательное',
            'sort' => 'Сортировка',
            'standart' => 'Стандартное',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookParams()
    {
        return $this->hasMany(BookParams::className(), ['tour_field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tours::className(), ['id' => 'tour_id']);
    }
}

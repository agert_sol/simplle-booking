<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tours".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Bookings[] $bookings
 * @property TourFields[] $tourFields
 */
class Tours extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Bookings::className(), ['tour_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourFields()
    {
        return $this->hasMany(TourFields::className(), ['tour_id' => 'id']);
    }

    /**
     * @return true
     */
    public function addStandartFields()
    {
    	$standart_fields = StandartFields::find()->all();
        foreach($standart_fields as $field){
			$new_field = new TourFields();
			$new_field->load(['TourFields' => [
				'tour_id' => $this->id,
				'title' => $field->title,
				'required' => $field->required,
				'sort' => $field->sort,
				'standart' => 1
			]]);
			$new_field->save();
        }
        return true;
    }
}

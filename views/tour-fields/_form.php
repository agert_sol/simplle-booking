<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TourFields */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-fields-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model && !$model->standart){ ?>

    <?= $form->field($model, 'tour_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'required')->checkbox(['uncheck' => 0]) ?>

    <?php } else { ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

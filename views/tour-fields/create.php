<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TourFields */

$this->title = 'Добавить поле тура "'.$tour->title.'"';
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['/tours']];
$this->params['breadcrumbs'][] = ['label' => 'Поля тура "'.$tour->title.'"', 'url' => ['/tour-fields', 'tour_id' => $tour->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-fields-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

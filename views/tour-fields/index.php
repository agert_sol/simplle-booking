<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TourFieldsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поля тура "'.$tour->title.'"';
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['/tours']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-fields-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить поле тура', ['create', 'tour_id' => $tour->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'tour_id',
            'title',
            'sort',
            [
            	'attribute' => 'required',
            	'value' => function($model){
					return $model->required ? 'Да' : 'Нет';
            	}
            ],
            // 'standart',

            [
            	'class' => 'yii\grid\ActionColumn',
            	'buttons' => [
			        'delete' => function ($url, $model, $key) {
			        	$options = [
		                    'title' => Yii::t('yii', 'Delete'),
		                    'aria-label' => Yii::t('yii', 'Delete'),
		                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
		                    'data-method' => 'post',
		                    'data-pjax' => '0',
		                ];
			            return $model->standart === 1 ? '' : Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
			        },
			    ],
            ],
        ],
    ]); ?>

</div>

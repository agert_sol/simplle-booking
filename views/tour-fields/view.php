<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TourFields */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['/tours']];
$this->params['breadcrumbs'][] = ['label' => 'Поля тура "'.$tour->title.'"', 'url' => ['/tour-fields', 'tour_id' => $tour->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-fields-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if ($model && !$model->standart){ ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'tour_id',
            'title',
            'sort',
            [
            	'attribute'=>'required',
            	'value' => $model->required ? 'Да' : 'Нет'
            ],
            [
            	'attribute'=>'standart',
            	'value' => $model->required ? 'Да' : 'Нет'
            ],
        ],
    ]) ?>

</div>

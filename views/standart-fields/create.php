<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StandartFields */

$this->title = 'Добавление стандартного поля';
$this->params['breadcrumbs'][] = ['label' => 'Стандартные поля', 'url' => ['/standart-fields']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standart-fields-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

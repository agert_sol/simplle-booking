<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StandartFields */

$this->title = 'Редактирование стандартного поля: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Стандартные поля', 'url' => ['/standart-fields']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="standart-fields-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BookParams */

$this->title = 'Create Book Params';
$this->params['breadcrumbs'][] = ['label' => 'Book Params', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-params-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BookParams */

$this->title = 'Update Book Params: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Book Params', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="book-params-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

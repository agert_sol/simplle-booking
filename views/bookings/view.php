<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bookings */

$this->title = 'Заказ №' . $model->id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    	$attributes = [
            'id',
            [
            	'attribute' => 'tour.title',
            	'label' => 'Название тура',
            ],
            'book_date',
        ];
        
        foreach($order_fields as $field){
			$attributes[] = [
				'label' => $field->tourField->title,
				'value' => $field->value,
			];
        }
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>
    

</div>

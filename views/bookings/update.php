<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bookings */

$this->title = 'Редактирование заказа №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказ №' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="bookings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'order_fields' => $order_fields,
        'toursList' => $toursList,
        'toursListOptions' => $toursListOptions,
        'date_params' => ['mask' => '9999-99-99']
    ]) ?>

</div>

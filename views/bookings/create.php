<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bookings */

$this->title = 'Добавление заказа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'order_fields' => $order_fields,
        'toursList' => $toursList,
        'toursListOptions' => $toursListOptions,
        'date_params' => $date_params,
    ]) ?>

</div>

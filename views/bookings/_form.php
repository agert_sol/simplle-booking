<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Bookings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bookings-form">

	<?php Pjax::begin(['enablePushState' => false]); ?>

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'tour_id', ['labelOptions' => ['label' => 'Название тура']])->dropDownList($toursList, $toursListOptions) ?>

    <?= $form->field($model, 'book_date')->widget(\yii\widgets\MaskedInput::className(), $date_params) ?>

	<?= Html::a(
        '',
        ['create'],
        ['class' => 'hidden', 'id' => 'getfields']
    ) ?>

    <?php foreach($order_fields as $order_field){
    	if (!$order_field->tourField->required){
			$order_field->scenario = 'empty_value';
    	}
    ?>

    <?= $form->field($order_field, '['. ($model->isNewRecord ? $order_field->tourField->id : $order_field->id) .']value', ['labelOptions' => ['label' => $order_field->tourField->title]])->textInput() ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<?php
$script = <<< JS
$(document).ready(function() {
    $('#bookings-tour_id').change(function(){
    	var urlParams = '?tour_id=' + this.value + '&user_date=' + $('#bookings-book_date').val();
    	$('#getfields').attr('href', $('#getfields').attr('href') + urlParams);
    	$('#getfields').click();
    });
});
JS;
$this->registerJs($script);
?>

    <?php Pjax::end(); ?>

</div>


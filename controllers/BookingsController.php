<?php

namespace app\controllers;

use Yii;
use app\models\Bookings;
use app\models\BookParams;
use app\models\Tours;
use app\models\TourFields;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * BookingsController implements the CRUD actions for Bookings model.
 */
class BookingsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bookings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Bookings::find()->joinWith('tour'),
            'sort' => [
            	'attributes' => [
				    'id' => [
		                'asc' => [
		                    'id' => SORT_ASC, 
		                ],
		                'desc' => [
		                    'id' => SORT_DESC
		                ],
		            ],
				    'tour.title' => [
		                'asc' => [
		                    'tours.title' => SORT_ASC, 
		                ],
		                'desc' => [
		                    'tours.title' => SORT_DESC
		                ],
		            ],
				    'book_date' => [
		                'asc' => [
		                    'book_date' => SORT_ASC, 
		                ],
		                'desc' => [
		                    'book_date' => SORT_DESC
		                ],
		            ],
			    ],
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bookings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$model = $this->findModel($id);
    	$order_fields = $this->getBookParams($model);
        return $this->render('view', [
            'model' => $model,
            'order_fields' => $order_fields,
        ]);
    }

    /**
     * Creates a new Bookings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($tour_id = null, $user_date = null)
    {
        $post = Yii::$app->request->post();
        $model = new Bookings();
        $toursList = ArrayHelper::map(Tours::find()->all(), 'id', 'title');
        $toursListOptions = ['prompt' => '--- Выберите тур ---'];
        
        $order_fields = [];
        if ($tour_id && ($tourModel = Tours::findOne($tour_id)) !== null) {
        	$model->tour_id = $tour_id;
        	$tour_fields = $tourModel->getTourFields()
        		->orderBy('`'.TourFields::tableName().'`.`sort`')
    			->all();
    		foreach($tour_fields as $tour_field){
				$order_field = new BookParams();
				$field_params = ['tour_field_id' => $tour_field->id];
				if (isset($post['BookParams']) && isset($post['BookParams'][$tour_field->id])){
					$field_params['value'] = $post['BookParams'][$tour_field->id]['value'];
				}
				$order_field->load(['BookParams' => $field_params]);
				$order_fields[] = $order_field;
    		}
        }

        if ($model->load($post) && $model->save()) {
        	if (isset($post['BookParams'])){
        		foreach($post['BookParams'] as $param_id => $params){
					if (($fieldsModel = TourFields::findOne($param_id)) !== null){
						$paramsModel = new BookParams();
						if (!$fieldsModel->required){
							$paramsModel->scenario = 'empty_value';
						}
						$params['book_id'] = $model->id;
						$params['tour_field_id'] = $fieldsModel->id;
						$paramsModel->load(['BookParams' => $params]);
						$paramsModel->save();
					}
        		}
        	}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
        	$date_params = ['mask' => '9999-99-99'];
        	if ($user_date && !$post) $date_params['options'] = ['value' => $user_date, 'class' => 'form-control'];
            return $this->render('create', [
                'model' => $model,
                'order_fields' => $order_fields,
                'toursList' => $toursList,
                'toursListOptions' => $toursListOptions,
        		'date_params' => $date_params,
            ]);
        }
    }

    /**
     * Updates an existing Bookings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $order_fields = $this->getBookParams($model);
        $toursList = [$model->tour->id => $model->tour->title];
        $toursListOptions = ['disabled' => 'disabled'];

        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
        	if (isset($post['BookParams'])){
        		foreach($post['BookParams'] as $param_id => $params){
					if (($paramsModel = BookParams::findOne($param_id)) !== null){
						if (!$paramsModel->tourField->required){
							$paramsModel->scenario = 'empty_value';
						}
						$paramsModel->load(['BookParams' => $params]);
						$paramsModel->save();
					}
        		}
        	}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'order_fields' => $order_fields,
                'toursList' => $toursList,
                'toursListOptions' => $toursListOptions,
            ]);
        }
    }

    /**
     * Deletes an existing Bookings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bookings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bookings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bookings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the BookParams model list for specified model.
     * @param object Bookings $model
     * @return array of BookParams for specified model
     */
    protected function getBookParams($model)
    {
        return $model->getBookParams()
    		->joinWith('tourField', '`'.TourFields::tableName().'`.`id` = `'.BookParams::tableName().'`.`tour_field_id`')
    		->orderBy('`'.TourFields::tableName().'`.`sort`')
    		->all();
    }
}

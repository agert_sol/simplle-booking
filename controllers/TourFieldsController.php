<?php

namespace app\controllers;

use Yii;
use app\models\Tours;
use app\models\TourFields;
use app\models\TourFieldsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TourFieldsController implements the CRUD actions for TourFields model.
 */
class TourFieldsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TourFields models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$queryParams = Yii::$app->request->queryParams;
    	if (isset($queryParams['tour_id'])
    			&& ($tour = Tours::findOne($queryParams['tour_id'])) !== null){
	        $searchModel = new TourFieldsSearch();
	        $dataProvider = $searchModel->search(['TourFieldsSearch' => ['tour_id' => $queryParams['tour_id']]]);
//	        $dataProvider->sort = [
//	        	'defaultOrder' => [
//			        'sort' => SORT_ASC,
//			    ],
//		    ];

	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	            'tour' => $tour,
	        ]);
        }
        else{
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single TourFields model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$model = $this->findModel($id);
    	if (($tour = $model->getTour()->one()) !== null){
	        return $this->render('view', [
	            'model' => $model,
	            'tour' => $tour,
	        ]);
        }
	    else{
			throw new NotFoundHttpException('Can\'t find main tour for field.');
	    }
    }

    /**
     * Creates a new TourFields model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$model = new TourFields();

	    if ($model->load(Yii::$app->request->post())) {
	    	if (($tour = $model->getTour()->one()) !== null && $model->save()){
	            return $this->redirect(['view', 'id' => $model->id]);
	        }
	        else{
				throw new NotFoundHttpException('Wrong tour id for field.');
	        }
	    } else {
	    	$queryParams = Yii::$app->request->queryParams;
		    if (isset($queryParams['tour_id']) && ($tour = Tours::findOne($queryParams['tour_id'])) !== null){
			    $model->load(['TourFields' => [
			        'tour_id' => $tour->id
			    ]]);
		        return $this->render('create', [
		            'model' => $model,
		            'tour' => $tour,
		        ]);
		    }
	        else{
				throw new NotFoundHttpException('The page requested in wrong way.');
	        }
	    }
    }

    /**
     * Updates an existing TourFields model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
	    	if (($tour = $model->getTour()->one()) !== null && $model->save()){
	            return $this->redirect(['view', 'id' => $model->id]);
	        }
	        else{
				throw new NotFoundHttpException('Can\'t set main tour for field.');
	        }
	    } else {
	    	if (($tour = $model->getTour()->one()) !== null){
	            return $this->render('update', [
	                'model' => $model,
	                'tour' => $tour,
	            ]);
            }
            else{
				throw new NotFoundHttpException('Can\'t find main tour for field.');
            }
        }
    }

    /**
     * Deletes an existing TourFields model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model && !$model->standart){
	        $tour = $model->getTour()->one();
	        $model->delete();

	        return $this->redirect(['/tour-fields', 'tour_id' => $tour->id]);
        }
        else{
			throw new NotFoundHttpException('Can\'t remove standart field.');
        }
    }

    /**
     * Finds the TourFields model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TourFields the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TourFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
